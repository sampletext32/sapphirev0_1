﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnMessageEndEdit : MonoBehaviour {
    private bool wasFocused;
    public void Update()
    {
        if (wasFocused && (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)))
        {
            Submit(gameObject.GetComponent<InputField>().text);
        }

        wasFocused = gameObject.GetComponent<InputField>().isFocused;
    }
    private void Submit(string Text)
    {
        Chat_Controller.OnSubmitInputClicked();
    }
}
