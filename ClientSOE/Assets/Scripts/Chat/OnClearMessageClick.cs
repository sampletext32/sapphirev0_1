﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClearMessageClick : MonoBehaviour
{
    public void OnButtonClick()
    {
        Chat_Controller.OnClearInputClicked();
    }
}