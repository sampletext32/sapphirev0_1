﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerControllerScript : MonoBehaviour {

    public static ServerControllerScript Instance;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Server_Controller.Connect();
            DontDestroyOnLoad(gameObject);
        }        
    }
}
