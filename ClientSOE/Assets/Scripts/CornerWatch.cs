﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CornerWatch : MonoBehaviour {
    Text goText;
	// Use this for initialization
	void Start () {
        goText = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        DateTime dt = DateTime.Now;
        goText.text = dt.Hour + ":" + dt.Minute + ":" + dt.Second;

	}
}
