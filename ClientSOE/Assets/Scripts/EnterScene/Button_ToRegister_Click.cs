﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Button_ToRegister_Click : MonoBehaviour {
    GameObject SystemMessage;
    Text MessageText;

    public void OnButtonClick()
    {
        SystemMessage = Instantiate(Resources.Load<GameObject>("Prefabs/SystemMessage"));
        MessageText = SystemMessage.transform.GetChild(1).gameObject.GetComponent<Text>();
        MessageText.text = "Загрузка сцены.\nПодождите.";

        //Привязать сообщение к канвасу текущей кнопки
        SystemMessage.transform.SetParent(gameObject.transform.parent);
        SceneManager.LoadScene("RegisterScene", LoadSceneMode.Single);
    }
}
