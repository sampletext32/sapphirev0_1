﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NicknameEnter : MonoBehaviour {
    public void EditTextEnd()
    {
        Registration_Controller.SetNickname(gameObject.GetComponent<InputField>().text);
    }
}
