﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PasswordEnter : MonoBehaviour {
    public void EditTextEnd()
    {
        Registration_Controller.SetPassword(gameObject.GetComponent<InputField>().text);
    }
}
