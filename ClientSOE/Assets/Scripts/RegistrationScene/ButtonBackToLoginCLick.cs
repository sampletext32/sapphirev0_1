﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonBackToLoginCLick : MonoBehaviour {
    GameObject SystemMessage;
    Text MessageText;
    public void OnButtonClick()
    {
        Registration_Controller.Reset();
        SystemMessage = Instantiate(Resources.Load<GameObject>("Prefabs/SystemMessage"));
        MessageText = SystemMessage.transform.GetChild(1).gameObject.GetComponent<Text>();
        MessageText.text = "Загрузка сцены.\nПодождите.";

        //Привязать сообщение к канвасу текущей кнопки
        SystemMessage.transform.SetParent(gameObject.transform.parent);
        SceneManager.LoadScene("LoginScene", LoadSceneMode.Single);
    }
}
