﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonDaleePress : MonoBehaviour {
    public void OnLoginAndPasswordClick()
    {
        Registration_Controller.ShowGenderAndNicknameCanvas();
    }
    public void OnSelectNicknameAndGenderClick()
    {
        Registration_Controller.ShowSelectSideCanvas();
    }
    public void OnSelectSideClick()
    {
        Registration_Controller.ShowClassCanvas();
    }
    public void OnSelectClassClick()
    {
        Registration_Controller.ShowConfirmCanvas();
    }
    public void OnConfirmRegisterClick()
    {
        Server_Controller.RegisterPlayer();
    }
}
