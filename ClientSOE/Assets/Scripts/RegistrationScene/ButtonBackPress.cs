﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBackPress : MonoBehaviour {    
    public void OnNicknameBackPress()
    {
        Registration_Controller.ShowLoginAndPasswordCanvas();
    }
    public void OnSelectSidePress()
    {
        Registration_Controller.ShowGenderAndNicknameCanvas();
    }
    public void OnClassBackPress()
    {
        Registration_Controller.ShowSelectSideCanvas();
    }
    public void OnConfirmBackPress()
    {
        Registration_Controller.ShowClassCanvas();
    }
}
