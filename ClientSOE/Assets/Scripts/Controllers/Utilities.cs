﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class Utilities
{
    public static Vector2 MeasureText(string Text)
    {
        GUIStyle style = new GUIStyle();

        style.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        style.fontSize = 12;  
              
        GUIContent content = new GUIContent();

        content.text = Text;

        Vector2 size = style.CalcSize(content);
        
        return size;
    }
}
