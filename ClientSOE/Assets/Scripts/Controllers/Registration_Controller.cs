﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class Registration_Controller {

    static string login;
    static string password;
    static string nickname;
    static bool gender;
    static string _class;
    static bool side;

    public static void SetLogin(string Login)
    {
        login = Login;
        Debug.Log("Login set to : " + Login);
    }

    public static void SetPassword(string Password)
    {
        password = Password;
        Debug.Log("Password set to : " + Password);
    }

    public static void SetGender(bool ngender)
    {
        gender = ngender;
        Debug.Log("Gender set to : " + ngender);
    }

    public static void SetClass(string nclass)
    {
        _class = nclass;
        Debug.Log("Class set to : " + nclass);
    }

    public static void SetSide(bool nside)
    {
        side = nside;
        Debug.Log("Side set to : " + nside);
    }

    public static void SetNickname(string Nickname)
    {
        nickname = Nickname;
        Debug.Log("Nickname set to : " + Nickname);
    }

    public static void ShowLoginAndPasswordCanvas()
    {
        GameObject.Find("Canvas_Input_Login_Password").GetComponent<Canvas>().enabled = true;
        GameObject.Find("Canvas_Choose_Gender_And_Nick").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Side").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Class").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Confirm_Register").GetComponent<Canvas>().enabled = false;
    }

    public static void ShowGenderAndNicknameCanvas()
    {
        GameObject.Find("Canvas_Input_Login_Password").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Gender_And_Nick").GetComponent<Canvas>().enabled = true;
        GameObject.Find("Canvas_Choose_Side").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Class").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Confirm_Register").GetComponent<Canvas>().enabled = false;
    }

    public static void ShowSelectSideCanvas()
    {
        GameObject.Find("Canvas_Input_Login_Password").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Gender_And_Nick").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Side").GetComponent<Canvas>().enabled = true;
        GameObject.Find("Canvas_Choose_Class").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Confirm_Register").GetComponent<Canvas>().enabled = false;
    }

    public static void ShowClassCanvas()
    {
        GameObject.Find("Canvas_Input_Login_Password").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Gender_And_Nick").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Side").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Class").GetComponent<Canvas>().enabled = true;
        GameObject.Find("Canvas_Confirm_Register").GetComponent<Canvas>().enabled = false;
    }

    public static void ShowConfirmCanvas()
    {
        GameObject.Find("Canvas_Input_Login_Password").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Gender_And_Nick").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Side").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Choose_Class").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Canvas_Confirm_Register").GetComponent<Canvas>().enabled = true;
    }

    public static NameValueCollection GetParameters()
    {
        NameValueCollection parameters = new NameValueCollection();
        parameters["login"] = login;
        parameters["password"] = password;
        parameters["nickname"] = nickname;
        parameters["gender"] = gender ? "1" : "0";
        parameters["class"] = _class;
        parameters["side"] = side ? "1" : "0";
        return parameters;
    }

    public static void OnRegisterSuccess()
    {
        Debug.Log("Регистрация прошла успешно");
    }

    public static void OnRegisterFail_LoginExist()
    {
        Debug.Log("Логин уже занят");
    }

    public static void Reset()
    {
        login = "";
        password = "";
        nickname = "";
        gender = true;
        _class = "";
        side = true;
    }
}
