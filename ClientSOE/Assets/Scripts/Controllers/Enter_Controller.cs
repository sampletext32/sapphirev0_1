﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class Login_Controller {

    static string Login;
    static string Password;
    public static void SetLogin(string NewLogin)
    {
        Login = NewLogin;
        Debug.Log("Login set to: " + NewLogin);
    }
    public static void SetPassword(string NewPassword)
    {
        Password = NewPassword;
        Debug.Log("Password set to: " + NewPassword);
    }
    public static NameValueCollection GetParameters()
    {
        NameValueCollection parameters = new NameValueCollection();
        parameters["login"] = Login;
        parameters["password"] = Password;
        return parameters;
    }

    public static void OnSuccessLogin()
    {
        Debug.Log("Вход выполнен");
    }

    public static void OnPasswordIncorrect()
    {
        Debug.Log("Неверный пароль");
    }

    public static void OnLoginNotExist()
    {
        Debug.Log("Пользователя не существует");
    }

    public static void Reset()
    {
        Login = "";
        Password = "";
    }
}
