﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageReceiverDropDown_Controller : MonoBehaviour
{
    static MessageReceiverDropDown_Controller Instance;
    static Text ActiveItemText;
    static bool IsActivated = false;

    public static void ActivateDefaultS()
    {
        Instance.OnItemSelected(1);
        Instance.OnButtonShowDropDownClick();
    }

    public void OnButtonShowDropDownClick()
    {
        if (!IsActivated)
        {
            gameObject.transform.GetChild(2).gameObject.SetActive(true);
            IsActivated = true;
        }
        else
        {
            gameObject.transform.GetChild(2).gameObject.SetActive(false);
            IsActivated = false;
        }
    }

    public void OnItemSelected(int ID)
    {
        switch (ID)
        {
            case 1:
                ActiveItemText.text = "Локация";
                break;
            case 2:
                ActiveItemText.text = "Группа";
                break;
            case 3:
                ActiveItemText.text = "Личное сообщение";
                break;
            case 4:
                ActiveItemText.text = "Торговля";
                break;
            case 5:
                ActiveItemText.text = "Клан";
                break;
        }
        OnButtonShowDropDownClick();
    }

    public void Start()
    {
        Instance = this;
        ActiveItemText = gameObject.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Text>();
    }
}
