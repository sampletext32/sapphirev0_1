﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chat_Controller : MonoBehaviour{
    static Chat_Controller Instance;
    private static GameObject MessagesContainer;
    private static InputField ChatInputField;
    private static Scrollbar ChatScrollBar;

    private static List<Chat_Message> Messages = new List<Chat_Message>();

    private static List<string> messages = new List<string>();

    private static int MaxTextWidth = 660;

    public void Start()
    {
        Instance = this;
        MessageReceiverDropDown_Controller.ActivateDefaultS();
        MessagesContainer = gameObject.transform.GetChild(1).GetChild(0).gameObject;
        ChatScrollBar = gameObject.transform.GetChild(2).gameObject.GetComponent<Scrollbar>();
        ChatInputField = gameObject.transform.GetChild(3).GetChild(1).gameObject.GetComponent<InputField>();        
    }
    
    public static void OnClearInputClicked()
    {

    }

    public static void OnSubmitInputClicked()
    {

    }

    public void ScrollBarValueChanged()
    {
        float Value = ChatScrollBar.value;
        float DeltaMessages = TotalTextLines - 20;
        if (DeltaMessages > 0)
        {
            float YOffset = (DeltaMessages * 18) * Value;
            MessagesContainer.transform.localPosition = new Vector3(0, -YOffset);
        }
    }

    private int TotalTextLines = 0;

    public void UpdateChatBlock()
    {
        while (MessagesContainer.transform.childCount > 0)
        {
            DestroyImmediate(MessagesContainer.transform.GetChild(0).gameObject);
        }
        int StartHeight = -180;

        TotalTextLines = 0;

        GameObject ChatMsgObj = Resources.Load<GameObject>("Prefabs/Msg");
        for (int i = Messages.Count - 1; i >= 0; i--)
        {
            string Message = Messages[i].Sender_Nickname + ": " + Messages[i].Content;
            Vector2 TextSize = Utilities.MeasureText(Message);

            int TextLines = (int)(TextSize.x / MaxTextWidth) + 1;

            GameObject MsgObject = Instantiate(ChatMsgObj);
            MsgObject.transform.SetParent(MessagesContainer.transform);

            MsgObject.transform.GetChild(0).gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, TextLines * 18);

            MsgObject.transform.localPosition = new Vector3(0, StartHeight + TotalTextLines * 18 + (TextLines / 2f) * 18);

            MsgObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = Message;

            TotalTextLines += TextLines;
        }
        ChatScrollBar.size = 20f / TotalTextLines;
    }
}