﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ClientDLL;
using SocketWrapper;
using System.Collections.Specialized;
using System.Linq;
using System;
using System.Text;

public class Server_Controller
{   
    private static Client cl = new Client();
    private static void OnMessageFromServer(object MessageObject)
    {
        MessageWrapper msgWrapper = MessageObject as MessageWrapper;
        string Command = msgWrapper.Data[0].ToString();
        switch (Command)
        {
            case "register_success":
                Registration_Controller.OnRegisterSuccess();
                break;
            case "register_err_login_exists":
                Registration_Controller.OnRegisterFail_LoginExist();
                break;
            case "login_success":
                Login_Controller.OnSuccessLogin();
                break;
            case "login_err_password_incorrect":
                Login_Controller.OnPasswordIncorrect();
                break;
            case "login_err_login_not_exists":
                Login_Controller.OnLoginNotExist();
                break;
            case "newGlobChatMsg":
                NameValueCollection parameters = NameValueCollectionFromString(msgWrapper.Data[1].ToString());
                break;
        }
    }
    private static void OnReceiveError(object MessageObject)
    {

    }
    private static void OnConnectionError(object MessageObject)
    {

    }
    private static void OnSendError(object MessageObject)
    {

    }

    public static string NameValueCollectionToString(NameValueCollection parameters)
    {
        string OutputString = "";
        for (int i = 0; i < parameters.Count; i++)
        {
            string KeyBase64 = Convert.ToBase64String(Encoding.ASCII.GetBytes(parameters.AllKeys[i]));
            string ValueBase64 = Convert.ToBase64String(Encoding.ASCII.GetBytes(parameters[i]));
            OutputString += KeyBase64 + "|" + ValueBase64 + "&" + (i != (parameters.Count - 1) ? "&" : "");
        }
        return OutputString;
    }
    public static NameValueCollection NameValueCollectionFromString(string StringParam)
    {
        NameValueCollection parameters = new NameValueCollection();
        string[] KeyValuePairs = StringParam.Split('&');
        for (int i = 0; i < KeyValuePairs.Length; i++)
        {
            string Key = Encoding.ASCII.GetString(Convert.FromBase64String(KeyValuePairs[i].Split('|')[0]));
            string Value = Encoding.ASCII.GetString(Convert.FromBase64String(KeyValuePairs[i].Split('|')[1]));
            parameters.Add(Key, Value);
        }
        return parameters;
    }

    public static void RegisterPlayer()
    {
        NameValueCollection parameters = Registration_Controller.GetParameters();
        string parametersStr = NameValueCollectionToString(parameters);
        if (cl.Connected)
        {
            cl.EnqueueNewMessage(new MessageWrapper("register", parametersStr));
        }
        else
        {
            Debug.Log("Not Connected");
        }
    }

    public static void LoginPlayer()
    {
        NameValueCollection parameters = Login_Controller.GetParameters();
        string parametersStr = NameValueCollectionToString(parameters);
        if (cl.Connected)
        {
            cl.EnqueueNewMessage(new MessageWrapper("login", parametersStr));
        }
        else
        {
            Debug.Log("Not Connected");
        }
    }

    public static void SendGlobalChatMessage(GlobalChatMessage msg)
    {
        NameValueCollection parameters = new NameValueCollection();
        parameters["sender_id"] = msg.Sender_ID.ToString();
        parameters["sender_nickname"] = msg.Sender_Nickname;
        parameters["content"] = msg.Content;
        string parametersStr = NameValueCollectionToString(parameters);
        cl.EnqueueNewMessage(new MessageWrapper("newGlobChatMsg", parametersStr));
    }

    public static void Connect()
    {
        cl.Connect("127.0.0.1", 11771);
        if (!cl.Connected)
        {
            Debug.Log("Не удалось подключиться к серверу");
        }
        else
        {
            Debug.Log("Соединение с сервером установлено");
        }
        cl.OnMessageFromServer = OnMessageFromServer;
        cl.OnReceiveError = OnReceiveError;
        cl.OnConnectionError = OnConnectionError;
        cl.OnSendError = OnSendError;
    }
}
