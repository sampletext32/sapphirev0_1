﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Chat_Message
{
    public int Sender_ID;
    public string Sender_Nickname;
    public string Content;
    public Chat_Message(int senderID, string senderNickname, string content)
    {
        this.Sender_ID = senderID;
        this.Sender_Nickname = senderNickname;
        this.Content = content;
    }
}

public class GlobalChatMessage : Chat_Message
{
    public GlobalChatMessage(int senderID, string senderNickname, string content) : base(senderID, senderNickname, content)
    {
    }
}
