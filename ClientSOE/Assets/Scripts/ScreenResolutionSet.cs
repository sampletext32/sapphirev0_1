﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenResolutionSet : MonoBehaviour {
    public int width;
    public int height;

	// Use this for initialization
	void Start () {
        Screen.SetResolution(width, height, false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
