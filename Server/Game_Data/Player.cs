﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Game_Data
{
    [Serializable]
    public class Player
    {
        //Side - NORTH=TRUE, SOUTH=FALSE

        //Gender - MAN=TRUE, WOMAN=FALSE

        //Type - (-1)-unconfirmed (-2)-banned 0-player 1-editor 2-support 10-admin
        public int id;
        public int type;
        public string login;
        public string password;
        public string nickname;
        public bool _gender;
        public int experience;
        public int money;
        public string _class;
        public bool _side;

        public string ToJSON()
        {
            StringBuilder builder = new StringBuilder();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.Serialize(this, builder);
            return builder.ToString();
        }
        public static Player FromJSON(string JSON)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Player p = serializer.Deserialize<Player>(JSON);
            return p;
        }

        public Player()
        {

        }
        public Player(int ID, int Type, string Login, string Password, string Nickname, bool Gender, int Experience, int Money, string Class, bool Side)
        {
            this.id = ID;
            this.login = Login;
            this.password = Password;
            this.nickname = Nickname;
            this._gender = Gender;
            this.experience = Experience;
            this.money = Money;
            this._class = Class;
            this._side = Side;
        }
    }
}
