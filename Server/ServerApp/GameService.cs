﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace ServerApp
{
    [ServiceContract(CallbackContract = typeof(IGameCallback))]
    interface IGameService
    {

    }
    interface IGameCallback
    {

    }
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.Single)]
    class GameService : IGameService
    {

    }
}
